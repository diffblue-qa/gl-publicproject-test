package org.crb;

import org.junit.Before;
import org.junit.Test;

/**
 * Created by carl on 10/07/17.
 */
public class SelectTest {

    Select s;

    @Before
    public void setUp(){
        s = new Select();
    }

    @Test
    public void SearchSelectTest(){
        assert(s.getOptions(1).contentEquals("Search"));
    }
}
