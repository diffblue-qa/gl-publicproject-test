package org.crb;

/**
 * Created by carl on 10/07/17.
 */
public class Select {

    public Select(){

    }


    public String getOptions(int option){

        switch( option ) {
            case 1:
                return "Search";
            case 2:
                return "Login";
            case 3:
                return "Exit";
            case 4:
                return "Secret Gateway";
            case 5:
                return "Trapped for Eternity";
            case 6:
                return "You have been killed";
            case 7:
                return "You are born again";
            case 8:
                return "You are Number 8";
            case 9:
                return "You are Number 9";
            case 10:
                return "You are Number 10";
            case 11:
                return "You are Number 11";
            case 12:
                return "You are Number 12";
            default:
                return "Try Again";
            }
        }

    public String getOptions1(int option){
	if (option < 0 ) {
		return "Negative" ;
	} else if (option == 0 ) {
		return "Zero";
	} else {
		return "Positive";
	}
    }

    public String getOptions2(int option){
	if (option < 10 ) {
		return "Small" ;
	} else if (option >=10 && option <= 20 ) {
		return "Medium";
	} else {
		return "Large";
	}
    }

}
